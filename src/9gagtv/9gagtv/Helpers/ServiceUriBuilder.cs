﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace _9gagtv.Helpers
{
    public class ServiceUriBuilder
    {
        private static readonly string NineGagTVApiBase = "http://9gag.tv/api/index/";
        private static readonly string[] PostTags = { "LJEGX","nJ1gX","1xqex", "OXA0x", "GXd6J", "1JgLx","Emp1J", "lmWgm",  "dJ4km", "yX5eX", "aJNBJ", "6Xwvx" };

        public enum PostType
        {
            NewPost,
            MostPopular,
            Prank, 
            MostPopularPrank,
            Cute,
            MostPopularCute,
            Music,
            MostPopularMusic,
            MovieAndTV,
            MostPopularMovieAndTV,
            NSFW,
            MostPopularNSFW
        }

        public string getNewPostQueryString(PostType type, int PostCount=20, string referenceTag="", int direction=1, int includeSelf=0)
        {
            

            var paramList = new Dictionary<string, string>();

            if (!string.IsNullOrEmpty(referenceTag))
                paramList.Add("ref_key", referenceTag);
            paramList.Add("count", PostCount.ToString());
            paramList.Add("direction", direction.ToString());
            paramList.Add("includeSelf", includeSelf.ToString());


            Debug.WriteLine(string.Concat(NineGagTVApiBase, PostTags[(int)type],"?", CreateNewPostQueryString(paramList)));

            return string.Concat(NineGagTVApiBase, PostTags[(int)type], "?", CreateNewPostQueryString(paramList));

           

        }

        private string CreateNewPostQueryString(Dictionary<string, string> Params)
        {
            var parameters = from paramKey in Params
                             select string.Format("{0}={1}", HttpUtility.UrlEncode(paramKey.Key), HttpUtility.UrlEncode(paramKey.Value));

            return string.Join("&", parameters);
        }

       
    }
}
