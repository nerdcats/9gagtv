using _9gagtv.Helpers;
using _9gagtv.Model;
using GalaSoft.MvvmLight;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Linq;
using System.Diagnostics;
using GalaSoft.MvvmLight.Command;
using System.Windows.Data;
using GalaSoft.MvvmLight.Messaging;
using MyToolkit.Multimedia;
using System.Windows;

namespace _9gagtv.ViewModel
{

    /// <summary>
        /// The <see cref="VideoCollection" /> property's name.
        /// </summary>
        
    
    public class MainViewModel : ViewModelBase
    {

        public const string MainMenuEntriesPropertyName = "MainMenuEntries";
        private ObservableCollection<string> _MainMenuEntries = null;
        public ObservableCollection<string> MainMenuEntries
        {
            get
            {
                return _MainMenuEntries;
            }

            set
            {
                if (_MainMenuEntries == value)
                {
                    return;
                }

                
                _MainMenuEntries = value;
                RaisePropertyChanged(MainMenuEntriesPropertyName);
            }
        }

        
        
        public const string VideoCollectionPropertyName = "VideoCollection";

        private ObservableCollection<VideoData> _videoCollection = null;

        
        public ObservableCollection<VideoData> VideoCollection
        {
            get
            {
                return _videoCollection;
            }

            set
            {
                if (_videoCollection == value)
                {
                    return;
                }

                
                _videoCollection = value;
                RaisePropertyChanged(VideoCollectionPropertyName);
            }
        }

        
        public const string MostPopularVideoCollectionPropertyName = "MostPopularVideoCollection";

        private ObservableCollection<VideoData> _mostPopularVideoCollection = null;

       
        public ObservableCollection<VideoData> MostPopularVideoCollection
        {
            get
            {
                return _mostPopularVideoCollection;
            }

            set
            {
                if (_mostPopularVideoCollection == value)
                {
                    return;
                }

                
                _mostPopularVideoCollection = value;
                RaisePropertyChanged(MostPopularVideoCollectionPropertyName);
            }
        }

   
        public const string DownloadFailedPropertyName = "DownloadFailed";

        private bool _downloadFailed = false;


        public bool DownloadFailed
        {
            get
            {
                return _downloadFailed;
            }

            set
            {
                if (_downloadFailed == value)
                {
                    return;
                }

               
                _downloadFailed = value;
                RaisePropertyChanged(DownloadFailedPropertyName);
            }
        }

      
        public const string PopularVideoDownloadFailedPropertyName = "PopularVideoDownloadFailed";

        private bool _popularVideoDownloadFailed = false;

    
        public bool PopularVideoDownloadFailed
        {
            get
            {
                return _popularVideoDownloadFailed;
            }

            set
            {
                if (_popularVideoDownloadFailed == value)
                {
                    return;
                }

                
                _popularVideoDownloadFailed = value;
                RaisePropertyChanged(PopularVideoDownloadFailedPropertyName);
            }
        }

        private ServiceUriBuilder _uriBuilder = new ServiceUriBuilder();

     
        public const string LoadNextPostsCommandPropertyName = "LoadNextPostsCommand";

        private RelayCommand _LoadNextPosts = null;
        public RelayCommand LoadNextPostsCommand
        {
            get
            {
                return _LoadNextPosts;
            }

            set
            {
                if (_LoadNextPosts == value)
                {
                    return;
                }

                
                _LoadNextPosts = value;
                RaisePropertyChanged(LoadNextPostsCommandPropertyName);
            }
        }


        public const string MainMenuChangedCommandPropertyName = "MainMenuChangedCommand";

        private RelayCommand<string> _mainMenuChanged = null;
        public RelayCommand<string> MainMenuChangedCommand
        {
            get
            {
                return _mainMenuChanged;
            }

            set
            {
                if (_mainMenuChanged == value)
                {
                    return;
                }

               
                _mainMenuChanged = value;
                RaisePropertyChanged(MainMenuChangedCommandPropertyName);
            }
        }

        
        public const string ShowVideoCommandPropertyName = "ShowVideoCommand";

        private RelayCommand<VideoData> _showVideoCommand = null;

        public RelayCommand<VideoData> ShowVideoCommand
        {
            get
            {
                return _showVideoCommand;
            }

            set
            {
                if (_showVideoCommand == value)
                {
                    return;
                }

                
                _showVideoCommand = value;
                RaisePropertyChanged(ShowVideoCommandPropertyName);
            }
        }

        
        public const string RefreshCommandPropertyName = "RefreshCommand";

        private RelayCommand<string> _refreshCommand = null;

        public RelayCommand<string> RefreshCommand
        {
            get
            {
                return _refreshCommand;
            }

            set
            {
                if (_refreshCommand == value)
                {
                    return;
                }

                _refreshCommand = value;
                RaisePropertyChanged(RefreshCommandPropertyName);
            }
        }

        public const string ProgressVisibilityPropertyName = "ProgressVisibility";

        private bool _ProgressVisibility = true;
        public bool ProgressVisibility
        {
            get
            {
                return _ProgressVisibility;
            }

            set
            {
                if (_ProgressVisibility == value)
                {
                    return;
                }

                _ProgressVisibility = value;
                RaisePropertyChanged(ProgressVisibilityPropertyName);
            }
        }


        public const string ProgressTextPropertyName = "ProgressText";

        private string _ProgressText = string.Empty;
        public string ProgressText
        {
            get
            {
                return _ProgressText;
            }

            set
            {
                if (_ProgressText == value)
                {
                    return;
                }

                _ProgressText = value;
                RaisePropertyChanged(ProgressTextPropertyName);
            }
        }

        public MainViewModel()
        {

            LoadNextPostsCommand = new RelayCommand(LoadNextPosts);
            MainMenuChangedCommand = new RelayCommand<string>(MainMenuChangedAction);
            ShowVideoCommand = new RelayCommand<VideoData>(ShowVideoAction);
            RefreshCommand = new RelayCommand<string>(RefreshAction);

            LoadMainMenuEntries();
            LoadData("Home");
        }

        public void SetProgressBarStatus( bool vis, string text="")
        {
            ProgressText = text;
            ProgressVisibility = vis;
        }

        private void RefreshAction(string key)
        {
            LoadData(key);
        }

        private async void ShowVideoAction(VideoData video)
        {
            try
            {
                await YouTube.PlayWithPageDeactivationAsync(video.videoExternalId, true, YouTubeQuality.QualityHigh);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Failed to fetch Youtube Link, kindly try later");
            }
        }

        private void MainMenuChangedAction(string param)
        {
            Messenger.Default.Send<string>("Close Main Menu", Constants.MenuClosingTag);
            LoadData(param);

            
        }

       

        public void LoadMainMenuEntries()
        {
            MainMenuEntries = new ObservableCollection<string>(Constants.MainMenuEntries.ToList());
        }

        public async void LoadData(string param)
        {
            DownloadFailed = false;
            PopularVideoDownloadFailed = false;

            SetProgressBarStatus(true, String.Format(Constants.LoadingStringFormat, param));

            param = param.Replace(" ", "");
            ServiceUriBuilder.PostType MainType;
            ServiceUriBuilder.PostType PopularType;

            if (param == "Home")
            {
                MainType = ServiceUriBuilder.PostType.NewPost;
                PopularType = ServiceUriBuilder.PostType.MostPopular;
            }
            else
            {

                MainType = (ServiceUriBuilder.PostType)System.Enum.Parse(typeof(ServiceUriBuilder.PostType), param, true);
                PopularType = (ServiceUriBuilder.PostType)System.Enum.Parse(typeof(ServiceUriBuilder.PostType), Constants.MostPopularTag + param, true);
            }

            try
            {        
                await LoadNewPosts(MainType);
            }
            catch
            {
                DownloadFailed = true;
                
            }

            try
            {
                await LoadMostPopularPosts(PopularType);
            }
            catch (Exception ex)
            {
                PopularVideoDownloadFailed = true;
              
            }
            SetProgressBarStatus(false);


        }

        private async Task LoadMostPopularPosts(ServiceUriBuilder.PostType PopularType)
        {
            MostPopularVideoCollection = null;
            var data = JsonConvert.DeserializeObject<ResponseObject>(await Downloader.GetStuffFromWeb(_uriBuilder.getNewPostQueryString(PopularType)));

            if (data.okay && data.meta.code == 200)
            {


                MostPopularVideoCollection = new ObservableCollection<VideoData>(data.data.posts);
                PopularVideoDownloadFailed = false;
            }
        }

        private async Task LoadNewPosts(ServiceUriBuilder.PostType MainType)
        {
            VideoCollection = null;
            var data = JsonConvert.DeserializeObject<ResponseObject>(await Downloader.GetStuffFromWeb(_uriBuilder.getNewPostQueryString(MainType)));

            if (data.okay && data.meta.code == 200)
            {


                VideoCollection = new ObservableCollection<VideoData>(data.data.posts);
                DownloadFailed = false;
            }
        }

        public async void LoadNextPosts()
        {
            try
            {
                if (VideoCollection != null && VideoCollection.Count > 0)
                {

                    var referencePost = VideoCollection.Last();
                    var data = JsonConvert.DeserializeObject<ResponseObject>(await Downloader.GetStuffFromWeb(_uriBuilder.getNewPostQueryString(ServiceUriBuilder.PostType.NewPost, 20, referencePost.prevPostId)));

                    if (data.okay && data.meta.code == 200)
                    {
                        foreach (var item in data.data.posts)
                        {
                            VideoCollection.Add(item);

                        }
                    }

                    DownloadFailed = false;

                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Can't load more posts");
            }
            
        }

       
    }
}