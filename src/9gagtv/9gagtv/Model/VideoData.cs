﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _9gagtv.Model
{
    public class Meta
    {
        public int code { get; set; }
    }

    public class VideoData
    {
        public string hashedId { get; set; }
        public string type { get; set; }
        public string title { get; set; }
        public string subTitle { get; set; }
        public string description { get; set; }
        public string ogTitle { get; set; }
        public string ogDescription { get; set; }
        public string sourceUrl { get; set; }
        public string canonical { get; set; }
        public string ogImageUrl { get; set; }
        public int createTimestamp { get; set; }
        public string labelPublishDate { get; set; }
        public string labelCreateTimestamp { get; set; }
        public string labelUpdateTimestamp { get; set; }
        public string content { get; set; }
        public string thumbnail_720w { get; set; }
        public string thumbnail_600w { get; set; }
        public string thumbnail_480w { get; set; }
        public string thumbnail_360w { get; set; }
        public string thumbnail_240w { get; set; }
        public string thumbnail_120w { get; set; }
        public string nextPostId { get; set; }
        public string prevPostId { get; set; }
        public string url { get; set; }
        public string shortenUrl { get; set; }
        public string nextPostUrl { get; set; }
        public string prevPostUrl { get; set; }
        public string timestampOrder { get; set; }
        public string externalView { get; set; }
        public string tweetCount { get; set; }
        public string fbLikeCount { get; set; }
        public string fbShareCount { get; set; }
        public string commentCount { get; set; }
        public string fbCommentCount { get; set; }
        public string videoId { get; set; }
        public string videoStarttime { get; set; }
        public string videoEndtime { get; set; }
        public string videoInterval { get; set; }
        public string videoExternalId { get; set; }
        public string videoExternalProvider { get; set; }
        public string videoDuration { get; set; }
        public string videoDurationFe { get; set; }
        public string videoSourceUrl { get; set; }
        public string videoExtension { get; set; }
        public string videoMediaId { get; set; }
        public string videoSubmissionUser { get; set; }
        public string photoCover { get; set; }
        public string photoCoverId { get; set; }
        public List<object> photoImages { get; set; }
        public List<object> photoIds { get; set; }
    }

    public class Data
    {
        public List<VideoData> posts { get; set; }
    }

    public class ResponseObject
    {
        public bool okay { get; set; }
        public Meta meta { get; set; }
        public Data data { get; set; }
    }
}
