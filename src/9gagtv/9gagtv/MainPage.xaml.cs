﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using _9gagtv.Resources;
using System.Windows.Data;
using SlideView.Library;
using System.Diagnostics;
using GalaSoft.MvvmLight.Messaging;
using _9gagtv.Helpers;
using System.Windows.Media.Animation;
using Telerik.Windows.Controls;
using MyToolkit.Multimedia;
using Microsoft.Phone.Tasks;
using Telerik.Windows.Controls.Reminders;

namespace _9gagtv
{
    public partial class MainPage : PhoneApplicationPage
    {
        private int lastIndex = 0;
        // Constructor
        public MainPage()
        {

            InitializeComponent();
            InteractionEffectManager.AllowedTypes.Add(typeof(RadDataBoundListBoxItem));

            Messenger.Default.Register<string>(this, Constants.MenuClosingTag, CloseMainMenu);


            RemindRatingNotification();


            
            //this.MainAutoHideBar.ScrollControl = MainFeedList;

            // Sample code to localize the ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private static void RemindRatingNotification()
        {
            RadRateApplicationReminder rateReminder = new RadRateApplicationReminder();
            rateReminder.RecurrencePerUsageCount = 5;
            rateReminder.AllowUsersToSkipFurtherReminders = true;
            rateReminder.MessageBoxInfo = new MessageBoxInfoModel()
            {
                Buttons = MessageBoxButtons.YesNo,
                Content = AppResources.RateReminderText,
                SkipFurtherRemindersMessage = AppResources.SkipRemindersText,
                Title = AppResources.RateReminderTitle
            };

            rateReminder.Notify();
        }

        private void CloseMainMenu(string obj)
        {
            MainSlideFrame.SelectedIndex = 1;
        }

        private void TopMenuButton_Click(object sender, RoutedEventArgs e)
        {
            if (MainSlideFrame.SelectedIndex == 1)
            {
                MainSlideFrame.SelectedIndex = 0;
               
            }
            else
            {
                MainSlideFrame.SelectedIndex = 1;
                

            }
            
        }

        private void MainSlideFrameSelectionChanged()
        {
            if (MainSlideFrame.SelectedIndex == 1)
            {
                if (this.ApplicationBars.SelectedIndex == 1)
                    this.ApplicationBars.SelectedIndex = 0;

                this.pivot.IsLocked = false;
                
                this.MainSlideFrame.IsSlideEnabled = false;
            }
            else
            {
                if(MainSlideFrame.SelectedIndex==2)
                {
                    this.ApplicationBars.SelectedIndex = 1;
                    //invoke new applicationbar
                }
                
                this.pivot.IsLocked = true;
                
                this.MainSlideFrame.IsSlideEnabled = true;

            }
        }

        protected override void OnBackKeyPress(System.ComponentModel.CancelEventArgs e)
        {
            if (YouTube.CancelPlay())
            {
                e.Cancel = true;
            }
            else
            {

                if (MainSlideFrame.SelectedIndex == 0 || MainSlideFrame.SelectedIndex == 2)
                {
                    MainSlideFrame.SelectedIndex = 1;
                    e.Cancel = true;
                }
                
            }

            base.OnBackKeyPress(e);
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {

            YouTube.CancelPlay();
            base.OnNavigatedTo(e);
        }

        private void ImageBar_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var multiplier = this.ImageBar.SelectedIndex;
            var ItemWidth = this.ImageBar.ActualWidth / this.ImageBar.Items.Count;

            DoubleAnimationUsingKeyFrames SlidingAnimation = this.MenuBorderSlidingStoryboard.Children.First() as DoubleAnimationUsingKeyFrames;
            var from = SlidingAnimation.KeyFrames.First() as EasingDoubleKeyFrame;
            var to = SlidingAnimation.KeyFrames.Last() as EasingDoubleKeyFrame;

            from.Value = (double)lastIndex * (double)ItemWidth;
            to.Value = (double)multiplier * (double)ItemWidth;

            lastIndex = multiplier;

            this.MenuBorderSlidingStoryboard.Begin();
        }

        private void MainSlideFrame_SelectionChanged(object sender, EventArgs e)
        {
            MainSlideFrameSelectionChanged();
        }

        private void AboutButton_Click(object sender, EventArgs e)
        {
            this.MainSlideFrame.SelectedIndex = 2;
        }

        private void ReviewButton_Click(object sender, EventArgs e)
        {
            MarketplaceReviewTask reviewTask = new MarketplaceReviewTask();
            reviewTask.Show();
        }

        private void Facebook_Click(object sender, EventArgs e)
        {
            ShowWebBrowserTask("http://touch.facebook.com/nerdcats");
        }

        private static void ShowWebBrowserTask(string uri)
        {
            WebBrowserTask webTask = new WebBrowserTask();
            webTask.Uri = new Uri(uri);
            webTask.Show();
        }

        private void Website_Click(object sender, EventArgs e)
        {
            ShowWebBrowserTask("http://teamnerdcats.com");
        }

        private void Store_Click(object sender, EventArgs e)
        {
            ShowWebBrowserTask("http://www.windowsphone.com/en-US/store/publishers?publisherId=NerdCats");
        }



        

       
       

        

        // Sample code for building a localized ApplicationBar
        //private void BuildLocalizedApplicationBar()
        //{
        //    // Set the page's ApplicationBar to a new instance of ApplicationBar.
        //    ApplicationBar = new ApplicationBar();

        //    // Create a new button and set the text value to the localized string from AppResources.
        //    ApplicationBarIconButton appBarButton = new ApplicationBarIconButton(new Uri("/Assets/AppBar/appbar.add.rest.png", UriKind.Relative));
        //    appBarButton.Text = AppResources.AppBarButtonText;
        //    ApplicationBar.Buttons.Add(appBarButton);

        //    // Create a new menu item with the localized string from AppResources.
        //    ApplicationBarMenuItem appBarMenuItem = new ApplicationBarMenuItem(AppResources.AppBarMenuItemText);
        //    ApplicationBar.MenuItems.Add(appBarMenuItem);
        //}
    }
}